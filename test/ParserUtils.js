// Dependencies
var async = require('async');
var Promise = require('bluebird');

module.exports = {
  // Iterates through each element of 'obj' and adds the result of 'parseFunc(element)' to 'accumulator'
  // Note that parseFunc must be of form: func(item, key, callback). Refer to https://github.com/caolan/async#forEachOf for additional
  parseForEachOf : function (accumulator, obj, parseFunc) {
    return new Promise(function parseForEachOfPromise(resolve, reject) {
      // Optimization spot: bind() vs closures
      // http://stackoverflow.com/questions/17638305/why-is-bind-slower-than-a-closure
      var acc = accumulator;

      async.forEachOf(obj, parseFunc.bind({accumulator : acc}), function asyncForEachOfCallback(error) {
        if (error) {
          // console.log(error);
          return reject(error);
        }

        // console.log('ACC', acc);
        return resolve(acc);
      });
    });
  }
};