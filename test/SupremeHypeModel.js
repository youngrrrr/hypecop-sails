// References:
// 1) http://www.evanmiller.org/deriving-the-reddit-formula.html
// 2) http://www.evanmiller.org/ranking-news-items-with-upvotes.html

// Dependencies
var ParserUtils = require('./ParserUtils');

// Constants
const LAMBDA = 0.0000115740; // seconds (1/ 86400)
const HYPECOP_EPOCH = 1452884933000; // milliseconds

module.exports = {
  applyHypeMultipliers : applyHypeMultipliers,
  calculateBaseValue: calculateBaseValue,
  calculateHypeMultiplier: calculateHypeMultiplier
};

function applyHypeMultipliers(items) {
  return ParserUtils.parseForEachOf([], items, applyHypeMultiplier);
}

function applyHypeMultiplier(item, index, callback) {
  item.hypeMultiplier = calculateHypeMultiplier(item);

  this.accumulator.push(item);

  return callback();
}

function calculateBaseValue(item, size) {
  var baseValue = 0.0;

  baseValue += factorType(item.type);

  baseValue += factorModel(item.model);

  baseValue += factorSize(size);

  return baseValue;
}

// Max: 4%
function factorType(type) {
  if (type.indexOf('sweatshirts') > -1) {
    return 4;
  } else if (type.indexOf('tops-sweaters') > -1) {
    return 3.5;
  } else if (type.indexOf('shirts') > -1) {
    return 3;
  } else if (type.indexOf('hats') > -1) {
    return 3;
  } else if (type.indexOf('shoes') > -1) {
    return 2.5; // vary based on collab?
  } else if (type.indexOf('accessories') > -1) {
    return 2;
  }

  return 0;
}

// Max: 3%
function factorModel(model) {
  if (model.indexOf('black') > -1) {
    return 3;
  } else if (model.indexOf('grey') > -1) {
    return 3;
  } else if (model.indexOf('navy') > -1) {
    return 3;
  }

  return 0;
}

// Max: 2%
function factorSize(size) {
  if (size.indexOf('Small') > -1) {
    return 2;
  } else if (size.indexOf('Medium') > -1) {
    return 1.5;
  } else if (size.indexOf('Large') > -1) {
    return 1;
  } else if (size.indexOf('XLarge') > -1) {
    return 0.5;
  }

  return 0;
}

// Hype factors, from most to least significant:
function calculateHypeMultiplier(sellCount, createdAt, baseValue, predictedValue) {
  var hypeValue = 0.0;

  hypeValue += factorSellCountAndAge(sellCount, createdAt);

  hypeValue += baseValue;

  hypeValue += predictedValue;

  return valueToMultiplier(hypeValue);
}

function factorSellCountAndAge(sellCount, createdAt) {
  var itemAge = (Date.now() - createdAt) / 1000;

  return hypeModel(sellCount, itemAge);
}

function hypeModel(sellCount, age) {
  if (sellCount < 0) {
    return;
  } else if (age <= 0) {
    return;
  }

  var lambdaAge = LAMBDA * age / 1000;

  return Math.log(Math.pow(1.25, sellCount)) - lambdaAge - Math.log(1 - Math.exp(-1 * lambdaAge));
}

function valueToMultiplier(value) {
  if (value < 0) {
    return 1;
  }

  return 1 + (value / 100);
}
