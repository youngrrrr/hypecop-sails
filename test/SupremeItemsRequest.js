// Dependencies
var Promise = require('bluebird');
var request = require('request');
var Utils = require('./Utils');

// Constants
const SUPREME_SHOP_URL = 'https://web.archive.org/web/20151216005534/https://www.supremenewyork.com/shop';
const NEW = '/new';
const ALL = '/all';
const JACKETS = '/all/jackets';
const PANTS = '/all/pants';
const HATS = '/all/hats';
const ACCESSORIES = '/all/accessories';
const SKATE = '/all/skate';

module.exports = {
  requestSupreme : requestSupreme,
  requestSupremeShopNew : Utils.partial(requestSupreme, NEW),
  requestSupremeShopAll : Utils.partial(requestSupreme, ALL),
  requestSupremeShopJackets : Utils.partial(requestSupreme, JACKETS),
  requestSupremeShopPants : Utils.partial(requestSupreme, PANTS),
  requestSupremeShopHats : Utils.partial(requestSupreme, HATS),
  requestSupremeShopAccessories : Utils.partial(requestSupreme, ACCESSORIES),
  requestSupremeShopSkate : Utils.partial(requestSupreme, SKATE)
};

function requestSupreme(categoryRoute) {
  return new Promise(function requestSupremePromise(resolve, reject) {
    request(SUPREME_SHOP_URL + categoryRoute, function requestCallback(error, response, body) {
      if (error) {
        return reject(error);
      }

      return resolve(body);
    });
  });
}