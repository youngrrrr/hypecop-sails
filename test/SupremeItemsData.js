// Dependencies
var async = require('async');
var Promise = require('bluebird');
var SupremeItemsRequest = require('./SupremeItemsRequest');
var SupremeShopParser = require('./SupremeShopParser');
var SupremeHypeModel = require('./SupremeHypeModel');

// Globals
var itemIdCounter = 0;
var itemPropertiesMap = {};
var itemCopInfoMap = {};
var itemHypeDataMap = {};
var itemHypeMultiplierMap = {};
var itemsOrderedByHype = {};

// Initializes and loads
function* init() {
  try {
    var newItemsHtml = yield SupremeItemsRequest.requestSupremeShopNew();

    var parsedItems = yield SupremeShopParser.parseHtml(newItemsHtml);

    // var finalItems = yield SupremeHypeModel.applyHypeMultipliers(parsedItems);

    // console.log(finalItems);

    initMaps(parsedItems);

    console.log('1', itemPropertiesMap);
    console.log('2', itemCopInfoMap);
    console.log('3', itemHypeDataMap);
    console.log('4', itemHypeMultiplierMap);
    console.log('5', itemsOrderedByHype);
  } catch (err) {
    console.log(err);
  }
}

// Updates based on CronJob and other information
function* update() {

}

function initMaps(parsedItems) {
  async.forEachOf(parsedItems, function (item, key, itemCallback) {
    async.forEachOf(item.sizes, function(sizeCode, size, sizeCallback) {
      itemIdCounter++; //put first in case async somehow messes with things...

      itemPropertiesMap[itemIdCounter] = {
        'imgUrl': item.imgUrl,
        'name': item.name,
        'model': item.model,
        'price': item.price,
        'size': size,
        'type': item.type,
        'keywords': [] //for now, in future modify to receive output of keywords function
      };

      itemCopInfoMap[itemIdCounter] = {
        'itemUrl': item.itemUrl,
        'sizeCode': sizeCode
      };

      itemHypeDataMap[itemIdCounter] = {
        'sellHistory': [],
        'createdAt': item.createdAt,
        'baseValue': SupremeHypeModel.calculateBaseValue(item, size),
        'speculatedValue': 0.0 //for now
      };

      itemHypeMultiplierMap[itemIdCounter] = SupremeHypeModel.calculateHypeMultiplier(0, item.createdAt, itemHypeDataMap[itemIdCounter].baseValue, 0.0);

      sizeCallback();
    });
    itemCallback();
  }, function (err) {
    if (err) {
      console.error(err.message);
    }

    itemsOrderedByHype = Object.keys(itemHypeMultiplierMap).sort(function(a,b){return itemHypeMultiplierMap[b] - itemHypeMultiplierMap[a]});
    console.log('*************************');
  });
}

Promise.coroutine(init)();
