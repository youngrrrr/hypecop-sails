module.exports = {

  isEmptyObject : function (obj) {
    for (var key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        return false;
      }
    }

    return true;
  },

  // Source: http://stackoverflow.com/a/321527/3905007
	// Another example: https://medium.com/@kbrainwave/currying-in-javascript-ce6da2d324fe
	partial : function (func /*, 0..n args */) {
		var args = Array.prototype.slice.call(arguments, 1);
		return function() {
			var allArguments = args.concat(Array.prototype.slice.call(arguments));
			return func.apply(this, allArguments);
		};
	}
}