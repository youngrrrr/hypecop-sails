// Dependencies
var Promise = require('bluebird');
var Cheerio = require('cheerio');
var ParserUtils = require('./ParserUtils');
var SupremeItemsRequest = require('./SupremeItemsRequest');
var Utils = require('./Utils');

// Constants
const WEB_ARCHIVE = 'https://web.archive.org';
const SECURE_PROTOCOL = 'https:';
const $_SHOP_ITEMS_SELECTOR = '#container article .inner-article a img';
const $_ITEM_NAME_SELECTOR = 'h1';
const $_ITEM_MODEL_SELECTOR = '.style';
const $_ITEM_PRICE_SELECTOR = '.price span';
const $_ITEM_SIZES_SELECTOR = '#size option';
const $_INFO_DIV_ID = '#details';
const $_SOLD_OUT_CONTAINER_SELECTOR = '#add-remove-buttons b';
const $_SOLD_OUT_CLASS = 'sold-out';
const ITEM_TYPE_INDEX = 7;

module.exports = {
  parseHtml : parseHtml
};

function parseHtml(itemsHtml) {
  var $ItemsHtml = Cheerio.load(itemsHtml);

  return parseCheerioItems($ItemsHtml($_SHOP_ITEMS_SELECTOR));
}

function parseCheerioItems($Cheerio) {
  return ParserUtils.parseForEachOf([], $Cheerio, parseCheerioItem);
}

function parseCheerioItem(elem, key, callback) {
  if (elem.next !== null) { //if true, item is sold out
    return callback();
  }

  var parseCheerioItemHelper = Utils.partial(parseCheerioItemGenerator, elem, key, callback);

  var _this = this;

  Promise.coroutine(parseCheerioItemHelper)()
    .then(function(item) {
      if (item) {
        _this.accumulator.push(item);
      }

      return callback();
    }
  );
}

// Helper function for parseCheerioItem
function* parseCheerioItemGenerator(elem, key, callback) {
  var item = {};

  item.itemUrl = elem.parent.attribs.href;

  var itemPage = yield SupremeItemsRequest.requestUrl(WEB_ARCHIVE + item.itemUrl); // yield SupremeItemsRequest.requestSupreme(item.itemUrl);

  var $ItemPage = Cheerio.load(itemPage);

  if ($ItemPage($_SOLD_OUT_CONTAINER_SELECTOR).hasClass($_SOLD_OUT_CLASS)) { //another check to see if item is sold out
    return null;
  }

  item.name = $ItemPage($_ITEM_NAME_SELECTOR, $_INFO_DIV_ID).text();
  item.model = $ItemPage($_ITEM_MODEL_SELECTOR, $_INFO_DIV_ID).text();
  item.price = $ItemPage($_ITEM_PRICE_SELECTOR, $_INFO_DIV_ID).text();
  item.imgUrl = WEB_ARCHIVE + elem.attribs.src; //SECURE_PROTOCOL + elem.attribs.src;
  item.type = item.itemUrl.split('/')[ITEM_TYPE_INDEX];

  if (item.type === 'accessories') {
    item.sizes = {};
    return item;
  }

  item.sizes = yield parseItemSizes($ItemPage($_ITEM_SIZES_SELECTOR, $_INFO_DIV_ID));

  item.createdAt = Date.now();

  return item;
}

function parseItemSizes(itemSizes) {
  return ParserUtils.parseForEachOf({}, itemSizes, parseItemSize);
}

function parseItemSize(elem, key, callback) {
  var size = elem.children[0].data;
  var val = elem.attribs.value;
  this.accumulator[size] = val;

  return callback();
}