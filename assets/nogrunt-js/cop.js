$("#submit-btn").click(function() {
	console.time("Time");
	var params = {
		"copForms": parseCopForms(),
		"billingName": $("#billing-name").val(),
		"email": $("#email").val(),
		"tel": $("#tel").val(),
		"billingAddress": $("#billing-address").val(),
		"billingAddress2": $("#billing-address-2").val(),
		"billingZip": $("#billing-zip").val(),
		"billingCity": $("#billing-city").val(),
		"billingState": $("#billing-state").val(),
		"billingCountry": $("#billing-country").val(),
		"ccType": $("#cc-type").val(),
		"ccNumber": $("#cc-number").val(),
		"ccMonth": $("#cc-month").val(),
		"ccYear": $("#cc-year").val(),
		"ccCvv": $("#cc-cvv").val()
	}

	$.post('/copsupreme', params, function(response) {
		console.timeEnd("Time");
		alert(response);
		// console.log("<p>" + response + "</p>");
		// $(this).parent().append("<p>" + response + "</p>");
	});
});

function parseCopForms() {
	var copForms = [];
	$(".cop-form-container .cop-form").each(function(copFormName, copFormValue) {
		var copForm = {};
		$.each(copFormValue.children, function(elementName, elementValue) {
			if (elementValue.className == "item-url" || elementValue.className == "size") {
				copForm[elementValue.className] = elementValue.value;
			}
		});
		copForms.push(copForm);
	});

	return copForms;
}
