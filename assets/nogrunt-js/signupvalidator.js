$(document).ready(function() {
	$('.form').validate({
		rules: {
			firstName: {
				required: true
			},
			lastName : {
				required: true
			},
			email: {
				required: true,
				email: true
			},
			password: {
				required: true,
				minlength: 6
			}
		},
		success: function(element) {
			element.text('OK!').addClass('valid');
		}
	});
});
