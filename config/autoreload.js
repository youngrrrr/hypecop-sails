/*
https://github.com/sgress454/sails-hook-autoreload
http://stackoverflow.com/questions/18687818/auto-reloading-a-sails-js-app-on-code-changes
*/

module.exports.autoreload = {
  active: true,
  usePolling: false,
  dirs: [
    "api/models",
    "api/controllers",
    "api/services"
  ]
};
