const supremeUrl = 'http://www.supremenewyork.com';
const secureProtocol = 'https:';
const pathPix188 = '/vi/';
const pathPix450 = '/ma/';
const pathPix1350 = '/zo/';
const infoDivId = '#details';

var cheerio = require('cheerio'),
request = require('request'),
async = require('async');

//module.exports = {
	//get: function() {
		request(supremeUrl + '/shop/all/accessories', function(err, res, body) {
			if (err) {
				console.error("1", err.message);
				throw err;
			}

			var $ = cheerio.load(body);

			var items = [];

			async.forEachOf($('#container article .inner-article a img'), function (elem, key, callback) {
				if (elem.next !== null) { //if not null, means sold_out_tag div exists, indicating that the item is sold out
					return callback();
				}

				var item = {};

				item.url = elem.parent.attribs.href;
				item.img = secureProtocol + elem.attribs.src;
				item.type = item.url.split('/')[2];

				console.log("TYPE:", item.type);

				async.waterfall([
					function(callback) {
						request(supremeUrl + item.url, function(err, res, body){
							if (err) {
								return callback(err);
							}

							var $ = cheerio.load(body);

							item.name = $('h1', infoDivId).text();
							item.model = $('.style', infoDivId).text();
							item.price = $('.price span', infoDivId).text().substring(1); //this assumes value given is $USD

							callback(null, $, item);
						});
					},
					function($, item, callback) {
						if (item.type === 'accessories') {
							item.sizes = {};
							return callback(null, item);
						}

						var sizes = {};
						async.forEachOf($('#size option', infoDivId), function (elem, key, callback) { //#cart-controls .add fieldset #size option
							var size = elem.children[0].data;
							var val = elem.attribs.value;
							sizes[size] = val;

							return callback();
						}, function (err) {
							if (err) {
								console.error("2", err.message);
								return callback(err);
							}

							item.sizes = sizes;
							return callback(null, item);
						});
					}
				], function (err, result) {
					if (err) {
						console.error("3", err.message);
						return;
					}

					items.push(result);
					/*if (!isEmptyObject(result.sizes)) { //checks to see if item is sold out
						console.log("TYPE:", result.type);
						
					}*/

					return callback();
				});
			}, function (err) {
				if (err) {
					console.error("4", err.message);
					return;
				}

				// console.timeEnd('Supreme');
				console.log(items);
				// return;
			});
		});
	//}
//}

function isEmptyObject(obj) {
	for (var key in obj) {
		if (Object.prototype.hasOwnProperty.call(obj, key)) {
			return false;
		}
	}
	return true;
}
