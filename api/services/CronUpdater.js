var peakingSupremeDropTime = '*/2 55-59 10 * * 4'; //Every other second from 10:55am - 10:59am on Thursdays
var peakSupremeDropTime = '* 0-5 11 * * 4'; //Every second from 11:00am - 11:05am on Thursdays
var testTime = '*/2 * * * * *';

var cronJob = require('cron').CronJob; //

module.exports = {
	supremeUpdaterJobs: {
		/*testJob: new cronJob({
			cronTime: testTime,
			onTick: function() {
				// var result = SupremeItemsDiff.get();
				// console.log("RESULT:", result[0]);
				console.log('Test');
			},
			start: true,
			timeZone: 'America/New_York'
		}),*/
		peakJob: new cronJob({
			cronTime: peakSupremeDropTime,
			onTick: function() {
				console.log('peakJob');
			},
			start: false,
			timeZone: 'America/New_York'
		}),
		peakingJob: new cronJob({
			cronTime: peakingSupremeDropTime,
			onTick: function() {
				console.log('peakingJob');
			},
			start: false,
			timeZone: 'America/New_York'
		})
	}
}