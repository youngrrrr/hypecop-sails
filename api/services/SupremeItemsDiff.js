/* Idea to make faster:
- Load to cheerio to filter out irrelevant info...?
*/

const supremeUrl = 'http://www.supremenewyork.com';

module.exports = {
	get: function() {
		sails.async.waterfall([
			function getTestPage(cb) {
				sails.request(supremeUrl + '/shop', function(err, res, body) {
					if (err) {
						console.error("1", err.message);
						throw err;
					}

					cb(null, body);
				});
			},
			function calculateDiff(prevPage, cb) {
				console.time('Diff');
				sails.request(supremeUrl + '/shop/new', function(err, res, body) {
					if (err) {
						console.error("2", err.message);
						throw err;
					}

					var args = {
						source: prevPage,
						diff: body,
						lang: "text"
					};

					var output = sails.prettydiff.api(args);

					cb(null, output);
				});
			}
		], function(err, result) {
			if (err) {
				console.error("", err.message);
			}

			return result;
		});
	}
}
