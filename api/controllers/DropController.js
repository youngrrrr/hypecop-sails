const supremeUrl = 'http://www.supremenewyork.com';
const secureProtocol = 'https:';
const pathPix188 = '/vi/';
const pathPix450 = '/ma/';
const pathPix1350 = '/zo/';
const infoDivId = '#details';

function isEmptyObject(obj) {
	for (var key in obj) {
		if (Object.prototype.hasOwnProperty.call(obj, key)) {
			return false;
		}
	}
	return true;
}

module.exports = {
	supreme : function(req, res1) {
		sails.request(supremeUrl + '/shop/new', function(err, res2, body) {
			if (err) {
				console.error("1", err.message);
				throw err;
			}

			var $ = sails.cheerio.load(body);

			var items = [];

			sails.async.forEachOf($('#container article .inner-article a img'), function (elem, key, callback) {
				var item = {};

				if (elem.next == null) {
					item.url = elem.parent.attribs.href;
					item.img = secureProtocol + elem.attribs.src;
					item.type = item.url.split('/')[2];

					sails.async.waterfall([
						function(callback) {
							sails.request(supremeUrl + item.url, function(err, res3, body) {
								if (err) {
									return callback(err);
								}

								var $ = sails.cheerio.load(body);

								item.name = $('h1', infoDivId).text();
								item.model = $('.style', infoDivId).text();
								item.price = $('.price span', infoDivId).text().substring(1); //this assumes value given is $USD

								callback(null, $, item);
							});
						},
						function($, item, callback) {
							if (item.type == 'accessories') {
								item.sizes = {};
								return callback(null, item);
							}

							var sizes = {};
							sails.async.forEachOf($('#cart-controls .add fieldset #size option', infoDivId), function (elem, key, callback) {
								var size = elem.children[0].data;
								var val = elem.attribs.value;
								sizes[size] = val;

								return callback();
							}, function (err) {
								if (err) {
									console.error("2", err.message);
									return callback(err);
								}

								item.sizes = sizes;
								return callback(null, item);
							});
						}
					], function (err, result) {
						if (err) {
							console.error("3", err.message);
							return;
						}

						if (!isEmptyObject(result.sizes)) { //checks to see if item is sold out
							items.push(result);
						}

						return callback();
					});
				} else {
					return callback();
				}
			}, function (err) {
				if (err) {
					console.error("4", err.message);
					return;
				}

				// console.timeEnd('Supreme');
				console.log(items);
				res1.send(items);
			});
		});
	}
}