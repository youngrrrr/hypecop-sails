/**
 * CopController
 *
 * @description :: Server-side logic for managing Cops
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 var SUPREME_URL = 'http://www.supremenewyork.com';
 var SUPREME_SECURE = 'https://www.supremenewyork.com';
 var SUPREME_CHECKOUT = '/checkout';

 var ORDER_SUCCESSFUL = 'Your order has been successfully processed';

 var FAILURE = 'Failure';
 var SUCCESS = 'Success';

 module.exports = {
 	supreme : function (req, res) {
 		/* ***** ADD A CHECK TO MAKE SURE INPUTS ARE NOT MALICIOUS!!!! ***** */
 		var copForms = req.param('copForms');
 		var billingName = req.param('billingName');
 		var email = req.param('email');
 		var tel = req.param('tel');
 		var billingAddress = req.param('billingAddress');
 		var billingAddress2 = req.param('billingAddress2');
 		var billingZip = req.param('billingZip');
 		var billingCity = req.param('billingCity');
 		var billingState = req.param('billingState');
 		var billingCountry = req.param('billingCountry');
 		var ccType = req.param('ccType');
 		var ccNumber = req.param('ccNumber');
 		var ccMonth = req.param('ccMonth');
 		var ccYear = req.param('ccYear');
 		var ccCvv = req.param('ccCvv');
 		/* http://code.tutsplus.com/tutorials/working-with-data-in-sailsjs--net-31525 @ 'sails policies' */
 		/* If all someone has to do is modify a JavaScript variable to control another user's account, you'll have a major problem. */

 		var response = "";

 		sails.async.each(copForms, function(copForm, cb) {
 			/* ***** ADD A CHECK TO MAKE SURE INPUTS ARE NOT MALICIOUS!!!! ***** */
 			var itemUrl = copForm['item-url'];
 			var size = copForm['size'];

 			sails.async.waterfall([
	 			function scrapeItemPage(cb) {
	 				sails.request(itemUrl, function(err, res, body) {
	 					// console.time('1');
	 					if (err || res.statusCode != 200) {
	 						cb(err, putCartAction, cartAuthToken, sizeVal);
	 						return;
	 					}

	 					// console.time('Cheerio1');
	 					var $ = sails.cheerio.load(body); //consider loading a partial body to improve performance
	 					//(e.g. using substring??? - but make sure HTML is still valid)
	 					// console.timeEnd('Cheerio1');

	 					/* Unnecessary due to SupremeItems.js?
	 					var sizeVal = '';
	 					if (size === '') {
	 						sizeVal = $('input[name="size"]').val();
	 					} else {
	 						var sizesJson = $('#size').children();
	 						var isFound = false;
	 						for (var i = 0; i < sizesJson.length; i++) {
	 							var sizeJson = sizesJson.get(i);

	 							if (sizeJson.children[0].data === size) {
	 								sizeVal = sizeJson.attribs.value;
	 								isFound = true;
	 								break;
	 							}
	 						}

	 						if(!isFound) {
	 							return;
	 						}
	 					}
	 					*/

	 					var putCartAction = $('#cart-addf').attr('action');
	 					var cartAuthToken = $('input[name="authenticity_token"]').val();

	 					if (putCartAction && cartAuthToken && sizeVal) {
	 						// console.timeEnd('1');
	 						cb(null, putCartAction, cartAuthToken, sizeVal);
	 					}
	 				});
				},
				function putItemInSessionCart(putCartAction, cartAuthToken, sizeVal, cb) {
					// console.time('2a');
					var commitVal = 'add to cart';
					var utf8Val = true;
					// console.timeEnd('2a');

					sails.request({
						url: SUPREME_URL + putCartAction,
						method: 'POST',
						form: {
							authenticity_token : cartAuthToken,
							commit : commitVal,
							size : sizeVal,
							utf8 : utf8Val
						}
					}, function(err, res, body) {
						// console.time('2b');
						if (err || res.statusCode != 302) {
							cb(err, jar);
							return;
						}

						var supremeSessionCookie = res.headers['set-cookie'][2].split('; ')[0];
						var cookie = sails.request.cookie(supremeSessionCookie);
						var jar = sails.request.jar();
						jar.setCookie(cookie, SUPREME_URL);

						// console.timeEnd('2b');
						cb(null, jar);
					});
				},
				function visitSessionCheckoutPage(jar, cb) {
					sails.request({
						url: SUPREME_SECURE + SUPREME_CHECKOUT,
						method: 'GET',
						jar: jar
					}, function(err, res, body) {
						// console.time('3');
						if (err || res.statusCode != 200) {
							cb(err, jar, subtotal, total, checkoutAuthToken);
							return;
						}

						// console.time('Cheerio2');
				 		var $ = sails.cheerio.load(body); //consider loading a partial body to improve performance
						//(e.g. using substring??? - but make sure HTML is still valid)
						// console.timeEnd('Cheerio2');

				 		var subtotal = Number($('#subtotal').text().substring(1));
						var total = Number($('#total').text().substring(1));
	 					var checkoutAuthToken = $('input[name="authenticity_token"]').val();

	 					// console.timeEnd('3');
						cb(null, jar, subtotal, total, checkoutAuthToken);
					});
				},
				function checkoutSessionCart(jar, subtotal, total, checkoutAuthToken, cb) { //checkout
					sails.request({
						url: SUPREME_SECURE + SUPREME_CHECKOUT,
						method: 'POST',
						jar: jar,
						form : {
							utf8: true,
							authenticity_token: checkoutAuthToken,
							'order[billing_name]': billingName,
							'order[email]': email,
							'order[tel]': tel,
							'order[billing_address]': billingAddress,
							'order[billing_address_2]': billingAddress2,
							'order[billing_zip]': billingZip,
							'order[billing_city]': billingCity,
							'order[billing_state]': billingState,
							'order[billing_country]': billingCountry,
							same_as_billing_address: 1,
							store_credit_id: '',
							'credit_card[type]': ccType,
							'credit_card[cnb]': ccNumber,
							'credit_card[month]': ccMonth,
							'credit_card[year]': ccYear,
							'credit_card[vval]': ccCvv,
							'order[terms]': 0,
							'order[terms]': 1,
							hpcvv: ''
						},
						followAllRedirects: true
					}, function(err, res, body) {
						// console.time('4');
						if (err || res.statusCode != 200) {
							cb(err, res);
							return;
						}

						var orderStatus = FAILURE;
						if (body.indexOf(ORDER_SUCCESSFUL) > -1) {
							orderStatus = SUCCESS;
						}

						// console.timeEnd('4');
						console.log(itemUrl, body);
						cb(null, orderStatus);
					});
				}
			], function (err, result) {
				if (err) {
					console.log(err);
					return;
				}

				// console.log(result);
				response += itemUrl + ": " + result + "\n";
			});
 		}, function(err) { //final callback isn't being reached???????
 			if (err) {
 				console.log(err);
 			}

 			res.status(200).send(response); /*??? not being reached??? Make sure this line
 			doesn't break the thing: */
 		});
	}
};
