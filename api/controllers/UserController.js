/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	login: function(req, res) {
		User.findOne({
			email: req.param('email')
		}, function foundUser(err, user) {
			if (err) {
				return res.negotiate(err);
			}

			if (!user) {
				return res.notFound();
			}

			require('machinepack-passwords').checkPassword({
				passwordAttempt: req.param('password'),
				encryptedPassword: user.encryptedPassword,
			}).exec({
				error: function(err) {
					return res.negotiate(err);
				},

				incorrect: function() {
					return res.notFound();
				},

				success: function() {
					// Store user id in the user session
					req.session.me = user.id;

					return res.ok();
				},
			})
		});
	},

	logout: function(req, res) {
		User.findOne(req.session.me, function foundUser(err, user) {
			if (err) {
				return res.negotiate(err);
			}

			if (!user) {
				sails.log.verbose('Session refers to a user who no longer exists.');
				return res.backToHomePage();
			}

			// Wipe out the session (log out)
			req.session.me = null;

			// Either send a 200 oK or redirect to the home page
			return res.backToHomePage();
		});
	},

	signup: function(req, res) {
		// Encrypt a string using the BCrypt algorithm.
		require('machinepack-passwords').encryptPassword({
			password: req.param('password'),
			// difficulty: 10,
		}).exec({
			// An unexpected error occurred.
			error: function (err){
				return res.negotiate(err);
			},
			// OK.
			success: function (encryptedPassword){
				require('machinepack-gravatar').getImageUrl({
					emailAddress: req.param('email')
				}).exec({
					error: function(err) {
						return res.negotiate(err);
					},
					success: function(gravatarUrl) {
						User.create({
							firstName: req.param('firstName'),
							lastName: req.param('lastName'),
							email: req.param('email'),
							encryptedPassword: encryptedPassword,
							lastLoggedIn: new Date(),
							gravatarUrl: gravatarUrl
						}, function userCreated(err, newUser) {
							if (err) {
								console.log("err: ", err);
                				console.log("err.invalidAttributes: ", err.invalidAttributes)

                				// If this is a uniqueness error about the email attribute,
				                // send back an easily parseable status code.
				                if (err.invalidAttributes && err.invalidAttributes.email && err.invalidAttributes.email[0]
				                	&& err.invalidAttributes.email[0].rule === 'unique') {
				                	return res.emailAddressInUse();
				                }

								return res.negotiate(err);
							}

							// Log user in
							req.session.me = newUser.id;

							return res.json({
								id: newUser.id
							});
						});
					}
				});
			}
		});
	},

	/*'new': function(req, res) {
		res.view();
	},

	create: function(req, res, next) {
		User.create(req.params.all(), function userCreated(err, user) {
			if (err) {
				console.log(err);
				req.session.flash = {
					err: err
				}

				//redirect back to home page if error
				return res.redirect('/user/new');
			}

			// res.json(user);
			// req.session.flash = {};
			res.redirect('/user/show' + user.id);
		});
	},

	show: function(req, res, next) {
		User.findOne(req.param('id'), function foundUser(err, user) {
			if (err) {
				return next(err);
			}

			if (!user) {
				return next();
			}

			res.view({
				user: user
			});
		});
	},

	index: function(req, res, next) {
		User.find(function foundUsers(err, users) {
			if (err) {
				return next(err);
			}

			res.view({
				users: users
			});
		})
	},

	edit: function(req, res, next) {
		User.findOne(req.param('id'), function foundUser(err, user) {
			if (err) {
				return next(err);
			}

			if (!user) {
				return next();
			}

			res.view({
				user: user
			});
		});
	},

	update: function(req, res, next) {
		User.update(req.param('id'), req.params.all(), function userUpdated(err) {
			if (err) {
				return res.redirect('/user/edit/' + req.param('id'));
			}

			res.redirect('/user/show/' + req.param('id'));
		});
	}*/
};
